# keepass duplicated passwords

This software is used to detected and output which credentials have duplicated passwords inside a keepass database,
helping to prevent situations where the same credential pair is used for more than one website, minimizing the
possibility of credential compromises through leaks.

# Installation

It can be installed system-wide or to a user by running:

```ShellSession
python3 setup.py install [--user]
```

# Dependencies

* Python 3
* libkeepass
