import sys

from contextlib import contextmanager
from io import StringIO
from unittest import TestCase
from duplicates.duplicates import Duplicates

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class DuplicateTestCase(TestCase):
    
    def test_password_duplicates(self):
        with captured_output() as (out, _):
            dup = Duplicates('./test/resources/passwd-duplicated.kdbx', 'test')
            dup.alert_duplicated_passwd()
            out = out.getvalue().strip() 
        self.assertIn('The following services share the same password', out)

    def test_login_and_password_duplicates(self):
        with captured_output() as (out, _):
            dup = Duplicates('./test/resources/login-and-passwd-duplicated.kdbx', 'test')
            dup.alert_duplicated_login_and_passwd()
            out = out.getvalue().strip()
        self.assertIn('The following services share the same login + password combination', out)

    def test_no_password_duplicates(self):
        with captured_output() as (out, _):
            dup = Duplicates('./test/resources/noduplicates.kdbx', 'test')
            dup.alert_duplicated_passwd()
            out = out.getvalue().strip() 
        self.assertIn('No services found with the same password', out)

    def test_no_login_and_password_duplicates(self):
        with captured_output() as (out, _):
            dup = Duplicates('./test/resources/noduplicates.kdbx', 'test')
            dup.alert_duplicated_login_and_passwd()
            out = out.getvalue().strip()
        self.assertIn('No services found with the same login + password combination', out)