import subprocess
import sys

from contextlib import contextmanager
from io import StringIO
from duplicates.__main__ import main
from unittest import TestCase

@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err

class MainTestCase(TestCase):

    def run_with_stdout(self, args):
        return str(subprocess.run(args, stdout=subprocess.PIPE).stdout)
    
    def test_password_duplicates(self):
        sys.argv = ['bogus-arg0', './test/resources/passwd-duplicated.kdbx', '--pass', 'test']
        with captured_output() as (out, _):
            main()
            out = out.getvalue().strip()
        self.assertIn('The following services share the same password', out)

    def test_login_and_password_duplicates(self):
        sys.argv = ['bogus-arg0', './test/resources/login-and-passwd-duplicated.kdbx', '--pass', 'test']
        with captured_output() as (out, _):
            main()
            out = out.getvalue().strip()
        self.assertIn('The following services share the same login + password combination', out)

    def test_no_password_duplicates(self):
        sys.argv = ['bogus-arg0', './test/resources/noduplicates.kdbx', '--pass', 'test']
        with captured_output() as (out, _):
            main()
            out = out.getvalue().strip()
        self.assertIn('No services found with the same password', out)

    def test_no_login_and_password_duplicates(self):
        sys.argv = ['bogus-arg0',  './test/resources/noduplicates.kdbx', '--pass', 'test']
        with captured_output() as (out, _):
            main()
            out = out.getvalue().strip()
        self.assertIn('No services found with the same login + password combination', out)

    def check_only_duplicated_passwds(self):
        sys.argv = ['bogus-arg0', './test/resources/login-and-passwd-duplicated.kdbx', '--pass', 'test', '--check-pass']
        with captured_output() as (out, _):
            main()
            out = out.getvalue().strip()
        self.assertIn('The following services share the same password', out)
        self.assertNotIn('The following services share the same login + password combination', out)
        
    def check_only_duplicated_pairs(self):
        sys.argv = ['bogus-arg0', './test/resources/login-and-passwd-duplicated.kdbx', '--pass', 'test', '--check-pairs']
        with captured_output() as (out, _):
            main()
            out = out.getvalue().strip()
        self.assertIn('The following services share the same login + password combination', out)
        self.assertNotIn('The following services share the same password', out)

    def test_login_check_all(self):
        sys.argv = ['bogus-arg0', './test/resources/login-and-passwd-duplicated.kdbx', '--pass', 'test']
        with captured_output() as (out, _):
            main()
            out = out.getvalue().strip()
        self.assertIn('The following services share the same login + password combination', out)
        self.assertIn('The following services share the same password', out)