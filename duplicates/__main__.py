import argparse
import getpass
import sys

from .duplicates import Duplicates

def main():
    parser = argparse.ArgumentParser(description='Verifies duplicated credentials on keepass databases')

    parser.add_argument('file', help='path to keepass database')
    parser.add_argument('--check-passwd', help='verify only passwords', action='store_true')
    parser.add_argument('--check-pairs', help='verify only login and password pairs', action='store_true')
    parser.add_argument('--passwd', '-p', help='password for the keepass file')
    args = parser.parse_args()

    dup = Duplicates(args.file, args.passwd if args.passwd else getpass.getpass())

    if args.check_pairs and not args.check_passwd:
        dup.alert_duplicated_login_and_passwd()

    if args.check_passwd and not args.check_pairs:
        dup.alert_duplicated_passwd()

    # Default behaviour, show everything
    if not args.check_passwd and not args.check_pairs:
        dup.alert_duplicated_login_and_passwd()
        dup.alert_duplicated_passwd()

if __name__ == '__main__':
    main()
