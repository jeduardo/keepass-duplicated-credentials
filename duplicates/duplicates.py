import libkeepass

class Duplicates(object):
    """
    Utility to detect duplicated logins and passwords inside of Keepass databases.
    """

    def __init__(self, filename, password):
        """ Read the duplicates of login+password and password into memory for later output. """
        self.login_and_passwd = {}
        self.passwd = {}
        with libkeepass.open(filename, password=password) as kdb:
            removed_uuids = [uuid.text for uuid in kdb.obj_root.findall(
                './/DeletedObject/UUID')]

            for entry in kdb.obj_root.findall('.//Group/Entry'):
                uuid = entry.find('./UUID').text
                if uuid not in removed_uuids:
                    kv = {string.find(
                        './Key').text: string.find('./Value').text for string in entry.findall('./String')}
                    user = kv['UserName']
                    pwd = kv['Password']

                    self.insert_or_append(
                        self.login_and_passwd, '{0}{1}'.format(user, pwd), kv['Title'])
                    self.insert_or_append(self.passwd, pwd, kv['Title'])


    def insert_or_append(self, hashmap, key, value):
        """ Insert elements into existing dictionaries or append list in case the element already exists. """
        cached = hashmap.get(key)
        if not cached:
            hashmap[key] = [value]
        else:
            cached.append(value)


    def alert_doubled(self, hashmap, message_found, message_not_found):
        """ Detect duplicates in the array and alert accordingly. """
        found = False
        for _, value in hashmap.items():
            if value and len(value) > 1:
                print(message_found.format(', '.join(value)))
                found = True
        if not found:
            print(message_not_found)

    def alert_duplicated_login_and_passwd(self):
        """ Issue an alert in case a login and password pair is duplicates across services. """
   
        self.alert_doubled(self.login_and_passwd,
                           'The following services share the same login + password combination: {0}',
                           'No services found with the same login + password combination!')

    def alert_duplicated_passwd(self):
        """ Issue an alert in case passwords are duplicated across services. """
        self.alert_doubled(self.passwd,
                           'The following services share the same password: {0}',
                           'No services found with the same password!')
