from setuptools import setup

setup(
    name = 'keepass-duplicated-credentials',
    version = '1.0.0',
    packages = ['duplicates'],
    install_requires = [
        'libkeepass'
    ],
    entry_points = {
        'console_scripts': [
            'duplicates = duplicates.__main__:main'
        ]
    })